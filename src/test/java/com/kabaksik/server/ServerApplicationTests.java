package com.kabaksik.server;

import com.kabaksik.server.enums.Status;
import com.kabaksik.server.model.RocketDto;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.reactivestreams.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.Disposable;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Map;

@SpringBootTest(classes = ClientConfiguration.class)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class ServerApplicationTests {

    @Autowired
    private Mono<RSocketRequester> rSocketRequesterMono;

    private RSocketRequester rSocketRequester;

    ParameterizedTypeReference<Map<String, RocketDto>>  parameterizedTypeReference =
            new ParameterizedTypeReference<Map<String, RocketDto>>(){};

    @ParameterizedTest
    @ValueSource(strings = {"kabkas", "kap"})
    void contextLoads(String name) {
        rSocketRequester = rSocketRequesterMono.block();

        Flux<RocketDto> rocketDtoFlux = Flux
                .interval(Duration.ofMillis(200))
                .map(s -> new RocketDto(name, s, 5f, 0, Status.ALIVE));

        Flux<Map<String, RocketDto>> kabka = feed(rocketDtoFlux);

        Disposable disposable = kabka.subscribe(value -> {
            System.out.println("name:" + name + " data:" + value);
        });

        Mono.just("one")
                .delayElement(Duration.ofSeconds(1))
                .log()
                .block();

        s(disposable);

        Mono.empty().delaySubscription(Duration.ofSeconds(5)).block();

    }

    //@ShellMethod("Stop streaming messages from the server.")
    public void s(Disposable disposable) {
        if (null != disposable) {
            disposable.dispose();
        }
    }

    public Flux<Map<String, RocketDto>> feed(Flux<RocketDto> movingDto) {
        RSocketRequester.RetrieveSpec feedMarketData = rSocketRequester
                .route("enter")
                .data(movingDto);

        Flux<Map<String, RocketDto>> movingDtoFlux = feedMarketData.retrieveFlux(parameterizedTypeReference);

        return movingDtoFlux;
    }

}
