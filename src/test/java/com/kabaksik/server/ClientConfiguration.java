package com.kabaksik.server;

import io.rsocket.DuplexConnection;
import io.rsocket.transport.ClientTransport;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.rsocket.RSocketRequester;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;

@TestConfiguration
public class ClientConfiguration {

    @Bean
    Mono<RSocketRequester> rSocketRequester(RSocketRequester.Builder rSocketRequesterBuilder) {
        return rSocketRequesterBuilder
                .rsocketConnector(connector -> connector
                        .reconnect(Retry.fixedDelay(Integer.MAX_VALUE, Duration.ofSeconds(1))))
                .connectTcp("localhost", 7000);
    }
}
