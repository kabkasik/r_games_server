package com.kabaksik.server.controller;

import com.kabaksik.server.model.RocketDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Controller
@Slf4j
public class RSocketController {
    private static Map<String, RocketDto> moving = new HashMap<>();
    private static Map<String, RocketDto> moving1 = new HashMap<>();

    @MessageMapping("enter")
    public Flux<Map<String, RocketDto>> enter(Flux<RocketDto> movingDto) {
        AtomicReference<String> name = new AtomicReference<>();

        Disposable subscribe = movingDto.subscribe(consumer -> {
            System.out.println("income data:" + consumer);
            moving.put(consumer.getName(), consumer);
            name.set(consumer.getName());
        });

        return Flux
                .interval(Duration.ofMillis(300))
                .map(s -> moving)
                .doFinally(consumer -> {
                    subscribe.dispose();
                    System.out.println("doFinally data:" + name);
                    moving.remove(name.get());
                })
                ;

    }
}
