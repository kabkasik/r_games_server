package com.kabaksik.server.model;

import com.kabaksik.server.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RocketDto {
    private String name;
    private float x;
    private float y;
    private int color;
    private Status status;
}
